import React, {Component} from 'react';
import EditModal from './EditModal';
import {
    ListGroup,
    ListGroupItem,
    Button,
    Alert
} from 'reactstrap';
import {
    CSSTransition,
    TransitionGroup
} from 'react-transition-group';
import {connect} from 'react-redux';
import {getItems, deleteItem, editItem} from "../actions/itemActions";
import PropTypes from 'prop-types';

class ShoppingList extends Component {

    componentDidMount() {
        this.props.getItems();
    }

    onDeleteClick = (id) => {
        this.props.deleteItem(id);
    };

    onEditClick = (id) => {
        this.props.editItem(id);
    };

    render() {
        const {items} = this.props.item;

        const list = () => {
            if (items.length > 0) {
                return (
                    <ListGroup>
                        <TransitionGroup className="shopping-list">
                            {items.map(({_id, name}) => (
                                <CSSTransition key={_id} timeout={500} classNames="fade">
                                    <ListGroupItem>
                                        <Button
                                            className="remove-btn"
                                            color="danger"
                                            size="sm"
                                            onClick={this.onDeleteClick.bind(this, _id)}
                                        >&times;</Button>
                                        <EditModal name={name}/>
                                        {name}
                                    </ListGroupItem>
                                </CSSTransition>
                            ))}
                        </TransitionGroup>
                    </ListGroup>
                )
            } else {
                return (
                    <Alert color="primary">
                        No items on your shopping list
                    </Alert>
                )
            }
        };

        return (
            list()
        );
    }

}

ShoppingList.propTypes = {
    getItems: PropTypes.func.isRequired,
    item: PropTypes.object.isRequired
};

const mapStateToProps = (state) => ({
    item: state.item
});

export default connect(mapStateToProps, {getItems, deleteItem, editItem})(ShoppingList);